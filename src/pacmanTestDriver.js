// TODO: Implement Test Driver Facade object connecting to your model
const pacmanTestDriver = {
    driveCase: function () {
        return {
            ghostPosition: {
                columnIndex: 1,
                rowIndex: 1
            }
        }
    },
    COMPASS : {
        NORTH: "NORTH",
        SOUTH: "SOUTH",
        WEST: "WEST",
        EAST: "EAST"
    },
    RANDOM : {
        TOP: "TOP",
        BOTTOM: "BOTTOM",
        WEST: "WEST",
        EAST: "EAST"
    },
    GHOST_TYPE : {
        SEARCHER: "SEARCHER",
        TEMPERAMENTAL_SEARCHER: "TEMPERAMENTAL_SEARCHER",
        LAZY: "LAZY"
    }
};
module.exports = pacmanTestDriver;